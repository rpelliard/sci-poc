export $(cat secrets.env | xargs)
export DATADOG_API_KEY=${DD_API_KEY}
export DATADOG_SITE=datad0g.com
datadog-ci git-metadata upload

export GIT_COMMIT_SHA=$(git rev-parse HEAD)
docker-compose build
docker-compose up
