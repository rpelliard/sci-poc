package main

import (
	"errors"
	"fmt"
	"net/http"

	httptrace "gopkg.in/DataDog/dd-trace-go.v1/contrib/net/http"
	"gopkg.in/DataDog/dd-trace-go.v1/ddtrace/tracer"
)

func main() {
	rules := []tracer.SamplingRule{tracer.RateRule(1)}
	tracer.Start(
		tracer.WithSamplingRules(rules),
		tracer.WithService("sci-poc"),
		tracer.WithGlobalTag("version", "v1"),
		tracer.WithEnv("dev"),
	)
	defer tracer.Stop()

	// Create a traced mux router
	mux := httptrace.NewServeMux()
	// Continue using the router as you normally would.
	mux.HandleFunc("/hello", hello)
	mux.HandleFunc("/error", serveFile)
	http.ListenAndServe(":8080", mux)
}

func hello(w http.ResponseWriter, req *http.Request) {
	span, _ := tracer.StartSpanFromContext(req.Context(), "hello_span", tracer.AnalyticsRate(1))
	defer span.Finish(tracer.WithError(errors.New("span error")))

	fmt.Fprintf(w, "hello\n")
}

func serveFile(w http.ResponseWriter, req *http.Request) {
	result, err := myFunction()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
	fmt.Fprintf(w, "Result: %s\n", result)
}

func myFunction() (string, error) {
	return "", errors.New("fake error")
}
