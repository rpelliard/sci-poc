package main

import (
	"net/http"
	"time"
)

func main() {
	ticker := time.NewTicker(20 * time.Second)
	for ; true; <-ticker.C {
		go http.Get("http://server:8080/hello")
		go http.Get("http://server:8080/error")
	}
}
